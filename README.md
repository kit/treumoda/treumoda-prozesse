# TreuMoDa – Treuhandstelle für Mobilitätsdaten

![](img/logo.png)

Dieses Repository enthält die Prozesse, die im Rahmen des BMBF-geförderten Projekts [TreuMoDa](https://www.treumoda.de) erstellt wurden.
Das Projekt konzentriert sich auf die anonymisierte Weitergabe von Mobilitätsdaten über eine spezielle Treuhandstelle.

Die im Projektverlauf erarbeiteten Prozesse sind als [Camunda](https://camunda.com/)-Exporte als Business Process Model an Notation (BPMN) Dateien in diesem Repository verfügbar.

Weitere Informationen zum Projekt TreuMoDa sind im [Abschlussbericht](https://publikationen.bibliothek.kit.edu/1000172026) zu finden.

Das Projekt wurde unter Förderkennzeichen 16DTM112A vom Bundesministerium für Forschung und Bildung (BMBF) gefördert und mit Mitteln der Europäischen Union unter NextGenerationEU finanziert.


[[_TOC_]]


## Installation

Die Prozesse wurden mit dem Online-Tool Camunda erstellt.
Es ist also keine Installation erforderlich, sondern die Erstellung eines Accounts auf der Homepage von [Camunda](https://camunda.com/).


## Benutzung

Nach der Erstellung eines Accounts auf Camunda können die hier gespeicherten .bpmn-Dateien importiert und verwendet werden.

Alternativ können die .bpmn-Prozesse auch mit weiteren kompatibalen Tools bearbeitet werden.


## Ausführliche Beschreibung

### Abkürzungsverzeichnis

| Datenaustausch | DA |
| :-: | :-: |
| Datengeber | DG |
| Datennehmer | DN |
| personenbezogen | pb |
| Treuhandstelle | THS |


### Strukturplan Konzept

![](img/realistisch_vs_optimistisch.svg)


### Realistische Konzept: Prozesse der THS

Kerngeschäft: Anonymisieren und pseudonymisieren von Daten

Technische Beratung und rechtliche Information über den Datenaustausch

DG und DN sind sich über Datenaustausch einig

THS stellt Musterverträge für die Datennutzung bereit

Individuelle Anonymisierung

Kein Eigennutzen der THS an den Daten

Vermittlung von DG nur mit deren Einverständnis

Überprüfung der Anonymität weitergeleiteter Daten im Abonnement



Informationen zu gesetzlichen Rahmenbedingungen des Datenaustausches werden an Kunden der THS weitergeleitet

Informationen zur technischen Umsetzung von Anonymisierungsverfahren werden an den Kunden weitergeleitet \([https://bwsyncandshare\.kit\.edu/s/7ysJmnpfRTYGiS7](https://bwsyncandshare.kit.edu/s/7ysJmnpfRTYGiS7)\)

Informationsmöglichkeiten sind über die Website der THS erreichbar

Es wird ein Leitfaden zur Information zusammengestellt

Es könnten Tool auf der Website vorgestellt werden\, die unterstützen herauszufinden\, ob die pb\. Daten vorliegen \([https://www\.berd\-nfdi\.de/iva1/](https://www.berd-nfdi.de/iva1/)\)

Informationen werden durch THS\-Mitarbeiter persönlich geteilt\, beispielsweise in Gesprächen

Informieren wird als Service der THS angeboten und ist kostenlos


#### Prozessübersicht

Realistisches Konzept
![](img/prozesse.svg)

Aktualitätsprüfung = Methodenüberwachung


#### Information

Realistisches Konzept/Optimistisches Konzept
![](img/information.svg)


#### Beratung

* Eine rechtliche Beratung zu spezifischen Fragen kann nur durch einen mind\. Volljuristen durchgeführt werden
* Eine Beauftragung oder Einstellung eines Rechtsanwalts ist durch die THS nicht vorgesehen
* Daher kann nur zu technischen Problemstellungen beraten werden
* Fragen der Kunden könnten sein:
  * Wie können Daten für die Zwecke des Datennehmers \(oder Datengebers\) anonymisiert werden?
  * Erläuterungen zu Anonymisierungsverfahren: Wie anonymisiert TreuMoDa? Welche Verfahren nutzt TreuMoDa? Was sind die Hintergründe dieser Verfahren?
* Technische Beratung wird als Service der THS gesehen und ist kostenlos

![](img/beratung.svg)


#### Daten\-vermittler

Die THS speichert nach Erlaubnis der Datengeber die Stichprobe der anonymen Daten\, um sie für Vermittlungszwecke zu nutzen

Die anonyme Stichprobe wird dann als Testdatensatz bezeichnet

Mit der Erlaubnis des DGs zur Speicherung der Testdaten ist der DG auch mit den Vermittlungsanfragen durch die THS einverstanden

Ein DN kann die THS anfragen\, ob die THS passende DGs mit einem entsprechenden DS empfehlen kann

Die THS überprüft die Handelsregisternummer des DNs\, um sicherzustellen\, dass es eine echte Anfrage ist

Die THS prüft ihre DG\-Kartei und leitet\, wenn vorhanden\, passenden DS an DN weiter

Der DN prüft\, ob er solche Daten nutzen kann

Nach Bestätigung des DNs fragt THS bei DG nach\, ob es zu einem Datenaustausch zwischen DN und DG über THS geben kann

Realistisches Konzept
![](img/08_datenvermittler_realk.svg)


#### Moderator

Vermittelnde Aufgabe der THS zwischen DG und DN

Prüfung der Handelsregisternummer beider Parteien

Unterstützung bei der Aushandlung der Konditionen zum Datenaustausch

Unterstützung der THS bei Diskussion zur Verwendung von Anonymisierungsverfahren

Mitwirken bei der Kostendiskussion über den „Wert der Daten“

Einbringen des Vertragsmusters vom Datenerwerbsvertrages

Grobes Angebot der THS

Weiterleitung eines unverbindlichen Angebots in Form von Mustern und individueller Zusammenstellung möglicher Kosten durch die THS sind kostenfrei

THS muss sich am Ende an Weisungen des DGs halten


Struktur\, Inhalt und Besonderheiten\, die im Datenerwerbsvertrag und Auftragsverarbeitungsvertrag festgelegt werden:
* Datenerwerbsvertrag zwischen DG und DN\. Dieser Vertrag beinhalten\, dass THS als Auftragsdatenverarbeiter engagiert wird\.
* Auftragsdatenverarbeitungsvertrag zwischen den Verantwortlichen und THS \(Verantwortlich könnte DG sein oder DG&DN\)
* Kosten die für die THS durch die Auftragsverarbeitung entstehen\, werden vom DN gezahlt
* Der DN darf so oft er möchte die anonymisierte Stichprobe neu anfordern\, muss aber für jede Wdh einen Pauschalbetrag bezahlen
* Nach der Bestätigung der Anonymisierung durch den Datennehmer\, muss dieser so lange keine Änderungen im Anonymisierungsverfahren vorgenommen werden\, den kompletten anonymisierten Datensatz akzeptieren und dafür halten \(bezahlen\)
* Die Gesamtkosten für die THS werden in einer Rechnung am Ende des Prozesses für den DN zusammengesellt
* DG kann Stichprobe und komplett anonymisierten Datensatz mehrfach prüfen
* Haftungsklauseln:
  * Keine  <span style="color:#000000">Einigung zwischen THS und DG in der Stichprobenanonymisierung; THS oder DG kann/will nicht anonymisieren </span>  <span style="color:#000000"> </span>  <span style="color:#000000">Kündigung/Auflösung des Datenerwerbsvertrages zwischen DG und DN</span>
  * DN bezahlt unabhängig vom Erfolgt die THS \(falls DN er doch unzufrieden sein sollte unabhängig von Erfolg\)
  * <span style="color:#000000">Sollte der DG unbegründet die Weitergabe der von der THS anonymisierten Daten verweigern </span>  <span style="color:#000000"> muss DG zahlen \(anders formuliert: </span> Nach Anonymisierung des kompletten Datensatzes wird die Gesamtzahlung fällig\. Der DN ist haftbar aus sich der THS  Bei unbegründeter Ablehnung der Datenweitergabe durch DG\, sollte DG zahlen\)

![](img/10_moderator_realk.svg)


#### Anonymisierung

Nach Moderationsprozess

Zusammenstellung einer Stichprobe durch DG

Annotation des DGs und der THS der Stichprobe zum Vergleich \(ggf\. Korrekturmöglichkeiten bei THS\)

Annotation des kompletten Datensatzes bei THS

Individuelles Anonymisierungsverfahren durch die THS für den DN

Stichproben für DG und DN zur Qualitätsbewertung

Abfrage bei DG\, ob er Stichprobe zu Vermittlungszwecken an die THS weitergibt

Realistisches Konzept
![](img/01_anonymisierungsprozess_datentausch_realk.svg)

#### Auftragsdatenverarbeitungsvertrag

Realistisches Konzept
![](img/17_teilprozess_auftrags_vertrag_anonymisierung_realk.svg)

####   Stichprobenanonymisierung

Realistisches Konzept
![](img/15_teilprozess_stichprobenanonymisierung_realk.svg)

####   Anonymsierung

Realistisches Konzept
![](img/16_teilprozess_anonymisierung_realk.svg)

####   Datenaustausch

![](img/01_anonymisierungsprozess_datentausch_realk.svg)
Nach Moderationsprozess

Zusammenführen zweier Datensätze von zwei Datengebern

Das Zusammenführen soll unkenntlich machen\, von wem die Daten tatsächlich stammen

Der entstandene Gesamtdatensatz kann für weitere DNs bestimmt sein oder an die selben DGs zurückgeführt werden

Die THS hat in diesem Aufbau volle Einsicht in die Daten



Aktualitäts\-prüfung

Abonnement für Kunden zur Prüfung\, ob Daten aktuell noch anonym sind

THS erleichtert Einhaltung der DSGVO auf langer Sicht

__Technische Verfahren\, wenn:__

Besseres Modell zur Anonymisierung vorhanden:

Mit Rohdaten wäre ein einfacher Vergleich möglich zwischen dem „alten“ anonymisierten Datensatz und dem „neuen“ anonymisierten Datensatz möglich

Man könnte auch einfach den „alten“ anonymisierten Datensatz nochmal mit dem neuen Modell durchlaufen lassen \(dann aber kein Vergleich\)

Speicherung einer Stichprobe könnte für eine Rückmeldung schon ausreichen

Anmerkungen: bei einem neuen Verfahren: Es könnte das neue Verfahren an der einen Stelle besser sein als das alte und an anderer Stelle schlechter  Vergleich nötig \(man könnte auch anonymisierte Daten von DN anfordern nach Warnung?\)

Zusätzliche Informationen vorhanden\, die zur Reidentifikation genutzt werden könnten:

Aus technischer Sicht bisher keine Idee  Juristen? Klausel?

„bösartige“ Algorithmen die anonymen Daten gefährden:

Recherche und Update der Anonymisierungsverfahren

Zusätzliche Anmerkungen:

Die Prüfung und Erfassung des Standes der Technik kann nur über Recherche erfolgen \(„Die neuesten Anonymisierungsverfahren und Algorithmen sind meist auf Image Processing Konferenzen wie CVPR zu finden\.\.\.“\)

Tonus ca\. alle 1\-2 Monate nach Updates\, neuen Verfahren\, etc\. suchen

Die Prüfung erfolgt von der Person\, die in der THS auch für die Anonymisierungsverfahren zuständig ist

#### Aktualitäts\-prüfung

Realistisches Konzept
![](img/04_aktualit-tspr-fung-realk.svg)


### Optimistisches Konzept: weitere Prozesse der THS

- Speicherung von Rohdaten durch die THS
- Lizenzverträge mit den Datengebern
- Individuelle Anonymisierung
- Kein Eigennutzen der THS an den Daten
- Vermittlung von DS nur mit Einverständnis der DG
- Überprüfung der Anonymität weitergeleiter Daten im Abonnement


#### Daten\-vermittler

* Die Treuhandstelle vermittelt bei der THS gespeicherte Daten an einen Datennehmer und anonymisiert diese Daten nach den Vorgaben der DN
* Lizenzvereinbarung mit den Datengebern nötig\, um Daten zu speichern
* Die Treuhandstelle gibt Daten an Datennehmer je nach Absprach mit Datengebern
* Datengeber haben folgende Optionen: \(Inklusion in Lizenzvereinbarung\)
  * Speicherung der Rohdaten \+ Weitergabe an beliebige DN
  * Speicherung der Rohdaten \+ Weitergabe an bestimme DN \+ Anfrageoption durch die THS an DG für neue DN



DN aus THS\-Datenpool
Optimistisches Konzept
![](img/06_dn_aus_ths-datenpool_optik.svg)

Ein DN befragt die THS\, ob es einen für ihn passenden DS gibt

Die THS sucht im eigenen Datenpool nach passenden Daten

Die THS erstellt ein Angebot für ein bekanntes Anonymisierungsverfahren

Falls keine Daten vorhanden sind\, versucht die THS einen DG zu vermitteln  Folie 22


Vermittlung von Datengebern an DN\, die bei THS angegeben haben Daten nur an bestimmte DN zu teilen

Optimistisches Konzept
Daten\-vermittler
![](img/08_datenvermittler_realk.svg)



Zunächst wird eine Stichprobe der Daten vom DG lizenziert

Dadurch kann die THS prüfen\, ob diese Daten genutzt werden können\, beispielsweise kann auch eine Weiterleitung an DN passieren und durch DN eine Prüfung der Daten vollzogen werden

Die THS trägt dabei das Risiko\, die Daten zu kaufen und eventuell nicht weiterverkaufen zu können

Durch die Stichprobe soll das Risiko minimiert werden


DG in THS\-Datenpool
Optimistisches Konzept
![](img/07_dg-in-ths-datenpool-optik.svg)

Daten\-vermittler


#### Lizenzverhandlungen

* Unterscheidung zwischen den Optionen der Datenspeicherung an der THS
  * Speicherung der Rohdaten \+ Weitergabe an beliebige DN
  * Speicherung der Rohdaten \+ Weitergabe an bestimme DN \+ Anfrageoption durch die THS an DG für neue DN
* Absprache der Dauer der Speicherung

Optimistisches Konzept
![](img/09_lizenzverhandlungen-optik.svg)


#### Anonymisierung

* Anonymisierungsprozess auf Anfrage vom Datennehmer
* Inhalt der Anfrage \(Input in Prozess\): DN fragt nach Daten mit entsprechenden Informationen
* \-\-> Prüfung der THS\, ob solche Daten vorhanden sind oder DG vermitteln\, DN kann auch DG vorschlagen\, welchen THS dann selbst anspricht
* THS kauft auf eigenes Risiko Daten für den Datennehmer beim DG ab\, zunächst einmal eine Stichprobe und danach den kompletten Datensatz \-\-> Risiko nur für die Stichprobe
* Im Prozess sind folgende Pfade enthalten:
  * passende Rohdaten \+ passendes Anonymisierungsverfahren bei THS vorhanden
  * passende Rohdaten \+ kein entsprechendes Anonymisierungsverfahren bei THS vorhanden
  * keine passenden Rohdaten \+ passendes Anonymisierungsverfahren bei THS vorhanden
  * keine passenden Rohdaten \+ kein entsprechendes Anonymisierungsverfahren bei THS vorhanden
* Stichproben zur Qualitätssicherung für DN

Optimistisches Konzept
![](img/02_anonymisierungsprozess-optik.svg)


#### Aktualitäts\-prüfung

Abonnement für Kunden zur Prüfung\, ob Daten aktuell noch anonym sind

THS erleichtert Einhaltung der DSGVO auf langer Sicht

Im optimistischen Konzept nur für DG relevant?

Optimistisches Konzept
![](img/03_aktualit-tspr-fung-optik.svg)


## Fragen 


### Geklärte Fragen

* Bei der Aktualitätsprüfung:
  * Was passiert\, wenn der DG kein Abo hat\, aber der DN und wir feststellen\, dass die Daten überarbeitet werden müssen? \-\-> wie schaffen wir hier ein transparentes Konzept?  rechtlich kein Konflikt\, vllt nur Hinweis\, aber nicht Ergebnis zeigen
  * Was passiert\, wenn THS Stand der Technik verpasst?  Tragbares Risiko
  * Gibt es konkrete Empfehlungen der THS\, was getan werden muss\, wenn S\.d\.T\. sich ändert? Sollten weitere Empfehlungen im Ablaufdiagramm enthalten sein?  Anwendungskatalog: „Was muss ich tun bei…“


### Übrige Fragen

Was bedeutet Lizenzvertrag? – Gibt es für den DG überhaupt die Möglichkeit nicht mit jedem DN zu teilen\, wenn wir die Lizenz für die Daten haben \(Vergleich Software\-Lizenz\)


## Lizenz

Die Prozesse wurden vom Karlsruher Institut für Techologie (KIT) innerhalb des TreuMoDa-Projektes erstellt und werden unter Creative Commons [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) zur Verfügung gestellt.
