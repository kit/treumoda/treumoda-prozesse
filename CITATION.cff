# This CITATION.cff file was generated with cffinit.
# Visit https://bit.ly/cffinit to generate yours today!

cff-version: 1.2.0
title: 'Treuhandstelle für Mobilitätsdaten (TreuMoDa): Prozesse'
message: >-
  If you use this dataset, please cite it using the metadata
  from this file.
type: dataset
authors:
  - given-names: Vivien
    family-names: Geenen
    affiliation: Karlsruhe Institute of Technology (KIT)
    orcid: 'https://orcid.org/0009-0009-7899-8249'
  - given-names: Sophia
    family-names: Kaiser
    affiliation: Karlsruhe Institute of Technology (KIT)
  - given-names: Till
    family-names: Riedel
    email: till.riedel@kit.edu
    affiliation: Karlsruhe Institute of Technology (KIT)
    orcid: 'https://orcid.org/0000-0003-4547-1984'
  - given-names: Andreas
    family-names: Czech
    affiliation: Karlsruhe Institute of Technology (KIT)
    orcid: 'https://orcid.org/0000-0002-6895-0606'
identifiers:
  - type: doi
    value: 10.5281/zenodo.12541158
    description: Publication on Zenodo
repository-code: 'https://gitlab.kit.edu/kit/treumoda/treumoda-prozesse'
abstract: >-
  Dieses Repository enthält die Prozesse, die im Rahmen des
  BMBF-geförderten Projekts
  [TreuMoDa](https://www.treumoda.de) erstellt wurden. Das
  Projekt konzentriert sich auf die anonymisierte Weitergabe
  von Mobilitätsdaten über eine spezielle Treuhandstelle.

  Die im Projektverlauf erarbeiteten Prozesse sind als
  [Camunda](https://camunda.com/)-Exporte als Business
  Process Model an Notation (BPMN) Dateien in diesem
  Repository verfügbar.

  Weitere Informationen zum Projekt TreuMoDa sind im
  [Abschlussbericht](https://publikationen.bibliothek.kit.edu/1000172026)
  zu finden.

  Das Projekt wurde unter Förderkennzeichen 16DTM112A vom
  Bundesministerium für Forschung und Bildung (BMBF)
  gefördert und mit Mitteln der Europäischen Union unter
  NextGenerationEU finanziert.
keywords:
  - data trustee
  - TreuMoDa
  - mobility
  - BPMN
license: CC-BY-SA-4.0
date-released: '2024-12-23'